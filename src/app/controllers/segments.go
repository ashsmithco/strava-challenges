package controllers

import "github.com/revel/revel"

// Segments controller type
type Segments struct {
	InternalApp
}

// Explore GET /segments/explore
func (c Segments) Explore() revel.Result {
	return c.Render()
}
