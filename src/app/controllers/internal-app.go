package controllers

import (
	"github.com/ashsmith/strava-challenges/src/app/models"
	"github.com/ashsmith/strava-challenges/src/app/routes"
	"github.com/revel/revel"
	"github.com/strava/go.strava"
)

// InternalApp is our controller for internal application
type InternalApp struct {
	App
}

// CurrentAthlete represents the logged in user
var CurrentAthlete = models.Athlete{}

// Dashboard GET /dashboard
func (c InternalApp) Dashboard() revel.Result {
	athlete := c.athlete()
	organisedChallenges := getOrganisersChallenges(athlete)
	challengesEntered := getAthleteEnteredChallenges(athlete)

	return c.Render(athlete, organisedChallenges, challengesEntered)
}

// CheckAuth can be used to insure the user is logged in, and can access these pages
func (c InternalApp) CheckAuth() revel.Result {
	authToken := c.Session["AuthToken"]

	c.RenderArgs["IsLoggedIn"] = false

	if authToken == "" {
		c.Flash.Error("Please login first!")
		return c.Redirect(routes.App.Index())
	}

	c.RenderArgs["IsLoggedIn"] = true

	return nil
}

func (c InternalApp) athlete() models.Athlete {

	authToken := c.Session["AuthToken"]

	if authToken == "" {
		panic("No auth token!")
	}

	if CurrentAthlete.AuthToken == authToken {
		return CurrentAthlete
	}

	athlete := models.Athlete{}
	err := c.Txn.SelectOne(&athlete, "SELECT * FROM athlete WHERE auth_token = ? LIMIT 1", authToken)

	if err != nil {
		panic(err)
	}

	return athlete
}

func getOrganisersChallenges(athlete models.Athlete) []models.Challenge {
	var organiserChallenges []models.Challenge
	Dbm.Select(&organiserChallenges, "SELECT * FROM challenges WHERE organiser_id=?", athlete.ID)
	return organiserChallenges
}

func getAthleteEnteredChallenges(athlete models.Athlete) []models.Challenge {
	var challengesEntered []models.Challenge
	Dbm.Select(&challengesEntered, "SELECT challenges.* FROM challenges JOIN challenge_entries ON challenge_id = challenges.id WHERE challenge_entries.athlete_id = ? AND challenges.end_date > NOW()", athlete.ID)
	return challengesEntered
}

// StravaClient fetches a strava client from the user's auth token.
func (c InternalApp) StravaClient() *strava.Client {
	authToken := c.Session["AuthToken"]
	return strava.NewClient(authToken)
}
