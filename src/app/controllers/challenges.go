package controllers

import (
	"encoding/json"
	"html/template"
	"time"

	"github.com/ashsmith/strava-challenges/src/app/models"
	"github.com/ashsmith/strava-challenges/src/app/routes"
	"github.com/revel/revel"
)

// Challenges controller type
type Challenges struct {
	InternalApp
}

// Index GET /challenges
// TODO: Obey private/public status.
func (c Challenges) Index() revel.Result {
	results, err := c.Txn.Select(models.Challenge{},
		`SELECT * FROM challenges`)

	if err != nil {
		panic(err)
	}

	var challenges []*models.Challenge
	for _, r := range results {
		c := r.(*models.Challenge)
		c.Organiser, err = getAthleteByID(int64(c.OrganiserID))
		if err != nil {
			continue // Skip because we cannot find the organiser. Therefor it's invalid.
		}
		challenges = append(challenges, c)
	}
	return c.Render(challenges)
}

// New GET /challenges/new
func (c Challenges) New() revel.Result {
	return c.Render()
}

// Create POST /challenges
func (c Challenges) Create(challenge *models.Challenge) revel.Result {
	challenge.StartDate, _ = time.Parse("2006-01-02", c.Params.Get("challenge.StartDate"))
	challenge.EndDate, _ = time.Parse("2006-01-02", c.Params.Get("challenge.EndDate"))

	challenge.Validate(c.Validation)
	// Handle errors
	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		// return c.RenderText("HERE")

		return c.Redirect(routes.Challenges.New())
	}

	athlete, err := getAthlete(c.Session["AuthToken"])
	if err != nil {
		c.Flash.Error("We couldn't find you for some reason!")
		return c.Redirect(routes.App.Index())
	}

	challenge.OrganiserID = int64(athlete.ID)

	revel.ERROR.Print(challenge)
	Dbm.Insert(challenge)

	return c.Redirect(routes.Challenges.Show(challenge.ID))
}

// Show GET /challenges/:id
// TODO: Obey private/public status.
func (c Challenges) Show(id int) revel.Result {
	challenge, err := getChallenge(id)
	if err != nil {
		c.Flash.Error("Challenge does not exist!")
		c.Redirect(routes.Challenges.Index())
	}
	hasEntered := alreadyEntered(c.athlete(), challenge)
	leaderboard := getLeaderboard(challenge, c.athlete())
	isOrganiser := isOrganiser(challenge, c.athlete())
	// Fetch the route latlng points for our map
	challengeRoute := challenge.SegmentRoute(c.StravaClient(), int64(challenge.SegmentID))
	SegmentDataJSON, _ := json.Marshal(challengeRoute.Data)
	SegmentRoute := template.JS(SegmentDataJSON)
	// Fetch segment information for stats display.
	SegmentStats, _ := challenge.Segment(c.StravaClient(), int64(challenge.SegmentID))

	return c.Render(challenge, hasEntered, leaderboard, isOrganiser, SegmentRoute, SegmentStats)
}

// Update PUT /challenges/:id
// This is where a user has submitted the form to
// update the challenge. You need to make sure it's the owner,
// and all data validates correctly, before saving to the database.
// Segments cannot be changed, once an challenge has started
// so make sure that information cannot be changed.
func (c Challenges) Update(id int, challenge *models.Challenge) revel.Result {
	challenge.ID = id

	challenge.StartDate, _ = time.Parse("2006-01-02", c.Params.Get("challenge.StartDate"))
	challenge.EndDate, _ = time.Parse("2006-01-02", c.Params.Get("challenge.EndDate"))

	challenge.Validate(c.Validation)
	// Handle errors
	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(routes.Challenges.Edit(id))
	}

	// Does the challenge belong to the user?
	originalChallenge, err := getChallengeFromAthelete(id, c.Session["AuthToken"])
	if err != nil {
		c.Flash.Error("You cannot modify this challenge!")
		return c.Redirect(routes.Challenges.Index())
	}

	if !challenge.IsOpen() {
		c.Flash.Error("This challenge has already ended!")
		return c.Redirect(routes.Challenges.Show(challenge.ID))
	}

	challenge.ID = originalChallenge.ID
	challenge.OrganiserID = originalChallenge.OrganiserID
	challenge.SegmentID = originalChallenge.SegmentID

	count, err := Dbm.Update(challenge)
	if err != nil {
		revel.ERROR.Fatal(err)
	}
	if count == 0 {
		c.Validation.Keep()
		c.FlashParams()
		c.Flash.Error("No changes were made!")
		return c.Redirect(routes.Challenges.Edit(id))
	}

	c.Flash.Success("Your Challenge has been successfully updated!")
	return c.Redirect(routes.Challenges.Show(id))
}

// Edit GET /challenges/:id/edit
func (c Challenges) Edit(id int) revel.Result {
	challenge, err := getChallengeFromAthelete(id, c.Session["AuthToken"])

	if err != nil {
		c.Flash.Error("Challenge does not exist")
		return c.Redirect(routes.Challenges.Index())
	}

	if !challenge.IsOpen() {
		c.Flash.Error("You cannot edit a challenge that has ended.")
		return c.Redirect(routes.Challenges.Show(challenge.ID))
	}

	c.RenderArgs["challenge"] = challenge
	return c.Render(c.RenderArgs)
}

// Enter POST /challenges/:id/enter
// A user can enter a challenge
// TODO: Obey private/public status.
func (c Challenges) Enter(id int) revel.Result {
	challenge, err := getChallenge(id)
	if err != nil {
		c.Flash.Error("Challenge does not exist!")
		return c.Redirect(routes.Challenges.Index())
	}

	athlete, err := getAthlete(c.Session["AuthToken"])

	if alreadyEntered(athlete, challenge) {
		c.Flash.Error("You have already entered this challenge")
		return c.Redirect(routes.Challenges.Show(id))
	}

	if !challenge.IsOpen() {
		c.Flash.Error("This challenge has already ended!")
		return c.Redirect(routes.Challenges.Show(challenge.ID))
	}

	var challengeEntry = models.ChallengeEntry{ChallengeID: int64(challenge.ID), AthleteID: int64(athlete.ID), CurrentBest: 0}

	dbErr := c.Txn.Insert(&challengeEntry)
	if dbErr != nil {
		panic(dbErr)
	}

	c.Flash.Success("You have been entered successfully!")
	return c.Redirect(routes.Challenges.Show(challenge.ID))

}

// Leave POST /challenges/:id/leave
// Allows an athlete to leave a challenge
func (c Challenges) Leave(id int) revel.Result {
	athlete := c.athlete()
	challenge, err := getChallenge(id)
	if err != nil {
		c.Flash.Error("Challenge does not exist")
		return c.Redirect(routes.Challenges.Index)
	}

	if !alreadyEntered(athlete, challenge) {
		c.Flash.Error("You are not in this challenge")
		return c.Redirect(routes.Challenges.Show(id))
	}

	if !challenge.IsOpen() {
		c.Flash.Error("This challenge has already ended!")
		return c.Redirect(routes.Challenges.Show(challenge.ID))
	}

	entry := getChallengeEntry(challenge.ID, athlete.ID)
	Dbm.Exec("DELETE FROM challenge_entries WHERE challenge_id = ? AND athlete_id = ? ", entry.ChallengeID, entry.AthleteID)

	c.Flash.Success("You have been removed from this challenge")
	return c.Redirect(routes.Challenges.Show(id))
}

// Delete DELETE /challenges/:id
func (c Challenges) Delete(id int) revel.Result {
	return c.Redirect(routes.Challenges.Index())
}

func getChallenge(id int) (models.Challenge, error) {
	challenge := models.Challenge{}
	err := Dbm.SelectOne(&challenge, "SELECT * FROM challenges WHERE id=?", id)
	// revel.ERROR.Fatal(challenge)
	return challenge, err
}

func getChallengeFromAthelete(id int, athleteAuthToken string) (models.Challenge, error) {
	challenge := models.Challenge{}
	err := Dbm.SelectOne(&challenge, "SELECT challenges.* FROM challenges JOIN athlete ON athlete.id = challenges.organiser_id WHERE athlete.auth_token=? AND challenges.id=?", athleteAuthToken, id)
	return challenge, err
}

func getAthlete(authToken string) (models.Athlete, error) {
	athlete := models.Athlete{}
	err := Dbm.SelectOne(&athlete, "SELECT * FROM athlete WHERE auth_token=?", authToken)

	return athlete, err
}

func getAthleteByID(ID int64) (models.Athlete, error) {
	athlete := models.Athlete{}
	err := Dbm.SelectOne(&athlete, "SELECT * FROM athlete WHERE id=?", ID)

	return athlete, err
}

func alreadyEntered(athlete models.Athlete, challenge models.Challenge) bool {
	count, err := Dbm.SelectInt("SELECT count(*) FROM challenge_entries WHERE challenge_id = ? AND athlete_id = ?", challenge.ID, athlete.ID)
	if err != nil {
		panic(err)
	}
	return count == 1
}

func getChallengeEntries(challengeID int) []models.ChallengeEntry {
	var challengeEntries []models.ChallengeEntry
	Dbm.Select(&challengeEntries, "SELECT * FROM challenge_entries WHERE challenge_id = ? ORDER BY current_best ASC", challengeID)

	return challengeEntries
}

func getChallengeEntry(challengeID int, athleteID int) models.ChallengeEntry {
	challengeEntry := models.ChallengeEntry{}
	err := Dbm.SelectOne(&challengeEntry, "SELECT * FROM challenge_entries WHERE challenge_id = ? AND athlete_id = ? LIMIT 1", challengeID, athleteID)
	if err != nil {
		panic(err)
	}

	return challengeEntry
}

func getLeaderboard(challenge models.Challenge, athelete models.Athlete) map[int]models.Leaderboard {
	var challengeEntries []models.ChallengeEntry
	Dbm.Select(&challengeEntries, "SELECT * FROM challenge_entries WHERE challenge_id = ? ORDER BY current_best = 0, current_best ASC LIMIT 10", challenge.ID)

	leaderboard := map[int]models.Leaderboard{}

	for x, entry := range challengeEntries {
		athlete, _ := getAthleteByID(entry.AthleteID)
		leaderboard[x] = models.Leaderboard{
			Rank:    int64(x + 1),
			Athlete: athlete,
			Time:    (time.Duration(entry.CurrentBest) * time.Second).String(),
		}
	}

	return leaderboard
}

func isOrganiser(challenge models.Challenge, athlete models.Athlete) bool {
	return int64(challenge.OrganiserID) == int64(athlete.ID)
}
