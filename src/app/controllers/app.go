package controllers

import (
	"fmt"
	"net/http"
	"os"

	"github.com/ashsmith/strava-challenges/src/app/models"
	"github.com/revel/revel"
	"github.com/strava/go.strava"
)

// App is our controller for external application
type App struct {
	GorpController
}

// Index GET /
func (c App) Index() revel.Result {

	var authURL = auth().AuthorizationURL("state1", strava.Permissions.Public, true)

	return c.Render(authURL)
}

// OAuth POST /oauth
func (c App) OAuth() revel.Result {
	oauthVerifier := c.Params.Get("code")

	// No code provided? take them home!
	if len(oauthVerifier) == 0 {
		return c.Redirect(App.Index)
	}

	// Authorize with Strava to complete oAuth
	auth, err := auth().Authorize(oauthVerifier, http.DefaultClient)

	// Throw ugly errors.
	if err != nil {
		return c.RenderError(err)
	}

	user := c.createUserFromOauth(auth.Athlete, auth.AccessToken)

	// Save our token to the session
	c.Session["AuthToken"] = user.AuthToken

	return c.Redirect(InternalApp.Dashboard)
}

func auth() *strava.OAuthAuthenticator {
	var authenticator *strava.OAuthAuthenticator

	clientID, found := revel.Config.Int("strava.client_id")
	if !found {
		revel.ERROR.Fatal("Cound not find strava.client_id")
	}
	clientSecret, found := revel.Config.String("strava.client_secret")
	if !found {
		revel.ERROR.Fatal("Could not find strava.client_secret")
	}

	strava.ClientId = clientID
	strava.ClientSecret = clientSecret

	authenticator = &strava.OAuthAuthenticator{
		CallbackURL:            fmt.Sprintf("http://%s/oauth", os.Getenv("VIRTUAL_HOST")),
		RequestClientGenerator: nil,
	}

	return authenticator
}

// IsLoggedIn does what it says on the tin.
func (c App) IsLoggedIn() revel.Result {
	if authToken := c.Session["AuthToken"]; authToken == "" {
		c.Flash.Error("Please log in first")
		return c.Redirect(App.Index)
	}

	return nil
}

func (c App) createUserFromOauth(stravaAthlete strava.AthleteDetailed, authToken string) models.Athlete {

	// Try to find the user:

	var athlete models.Athlete

	athlete.FirstName = stravaAthlete.FirstName
	athlete.LastName = stravaAthlete.LastName
	athlete.Email = stravaAthlete.Email
	athlete.AuthToken = authToken
	athlete.StravaID = stravaAthlete.Id

	if athleteExists(authToken) {
		c.Txn.Update(&athlete)
	} else {
		c.Txn.Insert(&athlete)
	}

	c.Flash.Success("Welcome, " + athlete.String())

	return athlete
}

func getUserFromAuthToken(authToken string) models.Athlete {
	var athlete = models.Athlete{}
	Dbm.SelectOne(&athlete, "select * from athlete where auth_token=?", authToken)
	return athlete
}

func athleteExists(authToken string) bool {
	count, err := Dbm.SelectInt("select count(*) from athlete where auth_token=?", authToken)
	if err != nil {
		panic(err)
	}
	return count == 1
}
