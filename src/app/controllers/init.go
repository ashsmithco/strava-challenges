package controllers

import (
	"database/sql"
	"fmt"
	"strings"

	"github.com/ashsmith/strava-challenges/src/app/models"
	"github.com/go-gorp/gorp"
	// We need this shit
	_ "github.com/go-sql-driver/mysql"
	"github.com/revel/revel"
)

func init() {
	revel.OnAppStart(InitDb)
	revel.InterceptMethod((*GorpController).Begin, revel.BEFORE)
	revel.InterceptMethod((*InternalApp).CheckAuth, revel.BEFORE)
	revel.InterceptMethod((*GorpController).Commit, revel.AFTER)
	revel.InterceptMethod((*GorpController).Rollback, revel.FINALLY)

	revel.TemplateFuncs["not_empty"] = func(a string) bool {
		return strings.TrimSpace(a) != ""
	}
}

func getParamString(param string, defaultValue string) string {
	p, found := revel.Config.String(param)
	if !found {
		if defaultValue == "" {
			revel.ERROR.Fatal("Cound not find parameter: " + param)
		} else {
			return defaultValue
		}
	}
	return p
}

func getConnectionString() string {
	host := getParamString("db.host", "")
	port := getParamString("db.port", "3306")
	user := getParamString("db.user", "")
	pass := getParamString("db.password", "")
	dbname := getParamString("db.name", "auction")
	protocol := getParamString("db.protocol", "tcp")
	dbargs := getParamString("dbargs", "parseTime=true")

	if strings.Trim(dbargs, " ") != "" {
		dbargs = "?" + dbargs
	} else {
		dbargs = ""
	}
	return fmt.Sprintf("%s:%s@%s([%s]:%s)/%s%s",
		user, pass, protocol, host, port, dbname, dbargs)
}

// InitDb defines our db
func InitDb() {
	connectionString := getConnectionString()
	if db, err := sql.Open("mysql", connectionString); err != nil {
		revel.ERROR.Fatal(err)
	} else {
		Dbm = &gorp.DbMap{
			Db: db,

			Dialect: gorp.MySQLDialect{Engine: "InnoDB", Encoding: "UTF8"}}
	}
	// Defines the table for use by GORP
	// This is a function we will create soon.
	defineAtheleTable(Dbm)
	defineChallengeTable(Dbm)
	defineChallengeEntriesTable(Dbm)
	if err := Dbm.CreateTablesIfNotExists(); err != nil {
		revel.ERROR.Fatal(err)
	}
}

func defineAtheleTable(dbm *gorp.DbMap) {
	t := dbm.AddTableWithName(models.Athlete{}, "athlete").SetKeys(true, "id")
	t.ColMap("first_name").SetMaxSize(255)
	t.ColMap("last_name").SetMaxSize(255)
	t.ColMap("email").SetMaxSize(255)
	t.ColMap("auth_token").SetMaxSize(255)
	t.ColMap("strava_id").SetUnique(true)
}

func defineChallengeTable(dbm *gorp.DbMap) {
	t := dbm.AddTableWithName(models.Challenge{}, "challenges").SetKeys(true, "id")
	t.ColMap("organiser_id")
	t.ColMap("name")
	t.ColMap("start_date")
	t.ColMap("end_date")
	t.ColMap("activity_type").SetMaxSize(255)
	t.ColMap("segment_id")
	t.ColMap("is_public")
}

func defineChallengeEntriesTable(dbm *gorp.DbMap) {
	t := dbm.AddTableWithName(models.ChallengeEntry{}, "challenge_entries")
	t.ColMap("challenge_id")
	t.ColMap("athlete_id")
	t.ColMap("current_best")
	t.SetUniqueTogether("challenge_id", "athlete_id")
}
