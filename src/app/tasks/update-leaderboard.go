package tasks

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	"github.com/ashsmith/strava-challenges/src/app/models"
	"github.com/go-gorp/gorp"
	"github.com/revel/modules/jobs/app/jobs"
	"github.com/revel/revel"
	"github.com/strava/go.strava"
)

// Dbm Database connection
var Dbm *gorp.DbMap

// UpdateLeaderboard is a cron task to fetch updates to open challenges leaderboards
type UpdateLeaderboard struct {
}

// Run cron execution, fetch all
func (j UpdateLeaderboard) Run() {
	Dbm = InitDb()
	defer Dbm.Db.Close()

	// // Loop over athletes.
	var athletes = []models.Athlete{}
	Dbm.Select(&athletes, "SELECT * FROM athlete")

	for _, athlete := range athletes {
		client := strava.NewClient(athlete.AuthToken)
		segmentService := strava.NewSegmentsService(client)

		challengeEntries := getAthleteChallengeEntries(athlete)
		if len(challengeEntries) == 0 {
			continue // No entries means no attempts to lookup.
		}
		discoverLatestSegmentAttempts(challengeEntries, segmentService)

	}
}

func getAthleteChallengeEntries(athlete models.Athlete) []models.ChallengeEntry {
	var challengeEntries = []models.ChallengeEntry{}
	Dbm.Select(&challengeEntries, "SELECT challenge_entries.* FROM challenge_entries JOIN challenges ON challenges.id = challenge_entries.challenge_id WHERE athlete_id = ? AND start_date <= NOW() AND end_date >= NOW() ", athlete.ID)
	if len(challengeEntries) == 0 {
		return challengeEntries
	}
	for i, entry := range challengeEntries {
		var challenge = models.Challenge{}
		err := Dbm.SelectOne(&challenge, "SELECT * FROM challenges WHERE id = ?", entry.ChallengeID)
		if err != nil {
			revel.ERROR.Print(err)
			continue // Probably no record found because it's out of range.
		}
		challengeEntries[i].Athlete = athlete
		challengeEntries[i].Challenge = challenge
	}
	return challengeEntries
}

func discoverLatestSegmentAttempts(challengeEntries []models.ChallengeEntry, segmentService *strava.SegmentsService) {
	for _, entry := range challengeEntries {
		efforts, err := segmentService.ListEfforts(entry.Challenge.SegmentID).
			AthleteId(entry.Athlete.StravaID).
			DateRange(entry.Challenge.StartDate, entry.Challenge.EndDate).
			Do()

		if err != nil {
			revel.ERROR.Print(err)
			continue
		}
		for _, effort := range efforts {
			if effort.Segment.ActivityType.String() != entry.Challenge.ActivityType {
				continue
			}

			if int64(effort.MovingTime) < entry.CurrentBest || entry.CurrentBest == 0 {
				entry.CurrentBest = int64(effort.MovingTime)
				_, err := Dbm.Update(&entry)
				if err != nil {
					revel.ERROR.Print(err)
				}
			}
		}
	}
}

func init() {
	revel.OnAppStart(func() {
		jobs.Every(5*time.Minute, UpdateLeaderboard{})
	})

}

// InitDb connection
func InitDb() *gorp.DbMap {
	connectionString := getConnectionString()
	db, err := sql.Open("mysql", connectionString)
	if err != nil {
		panic(err)
	}
	dbm := &gorp.DbMap{
		Db: db,

		Dialect: gorp.MySQLDialect{Engine: "InnoDB", Encoding: "UTF8"}}

	defineChallengeEntriesTable(dbm)

	return dbm
}

func getParamString(param string, defaultValue string) string {
	p, found := revel.Config.String(param)
	if !found {
		if defaultValue == "" {
			revel.ERROR.Fatal("Cound not find parameter: " + param)
		} else {
			return defaultValue
		}
	}
	return p
}

func getConnectionString() string {
	host := getParamString("db.host", "")
	port := getParamString("db.port", "3306")
	user := getParamString("db.user", "")
	pass := getParamString("db.password", "")
	dbname := getParamString("db.name", "auction")
	protocol := getParamString("db.protocol", "tcp")
	dbargs := getParamString("dbargs", "parseTime=True")

	if strings.Trim(dbargs, " ") != "" {
		dbargs = "?" + dbargs
	} else {
		dbargs = ""
	}
	return fmt.Sprintf("%s:%s@%s([%s]:%s)/%s%s",
		user, pass, protocol, host, port, dbname, dbargs)
}
func defineChallengeEntriesTable(dbm *gorp.DbMap) {
	t := dbm.AddTableWithName(models.ChallengeEntry{}, "challenge_entries").SetKeys(false, "challenge_id") //.AddIndex("EntriesIndex", "Btree", []string{"challenge_id", "athlete_id"}).SetUnique(true)
	t.ColMap("challenge_id")
	t.ColMap("athlete_id")
	t.ColMap("current_best")
	t.SetUniqueTogether("challenge_id", "athlete_id")
}
