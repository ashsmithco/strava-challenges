package models

import "fmt"

// Athlete defines a user/athlete
type Athlete struct {
	ID        int    `db:"id" json:"id"`
	FirstName string `db:"first_name" json:"first_name"`
	LastName  string `db:"last_name" json:"last_name"`
	Email     string `db:"email" json:"email"`
	StravaID  int64  `db:"strava_id" json:"strava_id"`
	AuthToken string `db:"auth_token" json:"auth_token"`
}

func (a *Athlete) String() string {
	return fmt.Sprintf("%s %s", a.FirstName, a.LastName)
}
