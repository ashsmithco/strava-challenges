package models

import (
	"regexp"
	"time"

	"github.com/revel/revel"
	"github.com/strava/go.strava"
)

// Challenge defines a challenge
type Challenge struct {
	ID           int              `db:"id" json:"id"`
	Name         string           `db:"name" json:"name"`
	OrganiserID  int64            `db:"organiser_id" json:"organiser_id"`
	Organiser    Athlete          `db:"-" json:"athlete"`
	StartDate    time.Time        `db:"start_date" json:"start_date"`
	EndDate      time.Time        `db:"end_date" json:"end_date"`
	ActivityType string           `db:"activity_type" json:"activity_type"`
	SegmentID    int64            `db:"segment_id" json:"segment_id"`
	IsPublic     bool             `db:"is_public" json:"is_public"`
	Entries      []ChallengeEntry `db:"-" json:"entries"`
}

// ChallengeEntry defines athletes who have joined a challenge.
type ChallengeEntry struct {
	ChallengeID int64     `db:"challenge_id" json:"challenge_id"`
	Challenge   Challenge `db:"-"`
	AthleteID   int64     `db:"athlete_id" json:"athlete_id"`
	Athlete     Athlete   `db:"-"`
	CurrentBest int64     `db:"current_best" json:"current_best"`
}

// Validate all challenge properties
func (challenge *Challenge) Validate(v *revel.Validation) {
	v.Required(challenge.Name).Message("Name is required!")
	v.MaxSize(challenge.Name, 200).Message("Challenge name should be less than 200 characters")
	v.MinSize(challenge.Name, 4).Message("Challenge name should be more than 4 characters")

	v.Required(challenge.SegmentID).Message("You must provide a segment!")
	v.Check(challenge.SegmentID, SegmentValidator{})
	v.Required(challenge.StartDate).Message("You must provide a start date!")
	v.Required(challenge.EndDate).Message("You must provide a end date!")
	v.Required(challenge.ActivityType).Message("Activity type is required!")
	v.Match(challenge.ActivityType, regexp.MustCompile("^(Ride|Run|VirtualRide)$")).Message("Activity type must be either Ride, Virtual Ride or Run")
}

// IsOpen defines if a challenge is open for entries.
// TODO: Implement public/private logic too.
func (challenge *Challenge) IsOpen() bool {
	return challenge.EndDate.After(time.Now())
}

// SegmentRoute will return the
func (challenge *Challenge) SegmentRoute(client *strava.Client, segmentID int64) *strava.LocationStream {
	streamTypes := []strava.StreamType{strava.StreamTypes.Location}
	segmentStream, err := strava.NewSegmentStreamsService(client).
		Get(segmentID, streamTypes).Do()

	if err != nil {
		panic(err)
	}

	return segmentStream.Location
}

// Segment will return segment information from Strava
func (challenge *Challenge) Segment(client *strava.Client, segmentID int64) (*strava.SegmentDetailed, error) {
	return strava.NewSegmentsService(client).Get(segmentID).Do()
}

// SegmentValidator will validate if a segment exists.
type SegmentValidator struct {
	revel.Validator
}

func (v SegmentValidator) isSatisfied(id int64) bool {
	token, exists := revel.Config.String("strava.default_auth_token")
	if !exists {
		return false
	}
	client := strava.NewClient(token)
	_, err := strava.NewSegmentsService(client).Get(id).Do()
	if err != nil {
		return false
	}

	return true
}

// DefaultMessage returns a standard error message to the user
func (v SegmentValidator) DefaultMessage() string {
	return "Segment is not valid"
}
