package models

// Leaderboard type defines a row within the overal leaderboard.
// This will typically be populated by the ChallengeEntry DB
type Leaderboard struct {
	Rank    int64 `json:"rank"`
	Athlete `json:"athlete"`
	Time    string `json:"time"`
}
