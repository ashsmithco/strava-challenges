'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant');

var PATHS = {
    sass: {
        src: 'src/public/src/scss',
        dest: 'src/public/css'
    },
    images: {
        src: 'src/public/src/images',
        dest: 'src/public/images'
    }
}

// Compile scss to public/css.
gulp.task('sass', function () {
    return gulp.src(PATHS.sass.src + '/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(PATHS.sass.dest));
});

// Listen/Watch for changes for on the fly compiling.
gulp.task('sass:watch', function () {
    return gulp.watch(PATHS.sass.src + '/**/*.scss', ['sass']);
});

// Optimise images!
// @TODO: Sprite building
// @TODO: Only optimise images that have been modified recently
gulp.task('images', function() {
    return gulp.src(PATHS.images.src + '/*')
        .pipe(imagemin({
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			use: [pngquant()]
		}))
		.pipe(gulp.dest(PATHS.images.dest));
});


gulp.task("build", ['sass']);
gulp.task("default", ['sass:watch']); // Compile on the fly by default.
