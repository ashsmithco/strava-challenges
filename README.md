# Strava Custom Challenges

Strava custom challenges allow you and your friends to challenge each other on your favourite segments.

# How to get it running..

Before anything you will need to configure some environment variables. This project relies on the .env and .dbenv files for setting them. Refer to the .example files.

I'm using docker compose, so once docker, and docker-compose are installed all you need to do is run:

    docker-compose up -d #-d deamonizes so it can run in the background!
    # or for production:
    docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d

Production uses .env_prod and .dbenv_prod for defining environment variables as these should differ to your local installation. Again, use the .env.example and .dbenv.example as a basis.

Running in production is as simple as this:

    docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d

# Need to install docker?

OSX:

_Note: Install VirtualBox first if you do not have it_

    brew install docker
    brew install docker-compose
    brew install docker-machine

    docker-machine create -d virtualbox default
    docker-machine start default

    eval $(docker-machine env default)

    cd /project/root/
    docker-compose up -d # First time running this will require to fetch images, and build the 'app' and 'static-assets' containers.

On top of this the web app requires a VIRTUAL_HOST env variable defined, you will need to add this to your `/etc/hosts` file pointing to your docker machine IP (run docker-machine ip $DOCKER_MACHINE_NAME). So something like this:

    192.168.99.100 dev.stravachallenges.com

Then you can access the site in your browser.

# Local development notes

### Virtualbox issues
Unfortunately using virtualbox with vbfs (how virtualbox mounts files) means that you can experience issues with files not updating. For example, updating template files. You will need to restart the app container each time. Like so:

    docker-compose restart app

Usually revel (the web framework I'm using for Go) will reload file changes when in development mode.

### Sass and images..

I'm using Gulp, so run the following from the root of the project:
    npm install

Then to watch for sass changes, just run the gulp command:

    gulp

If you want to optimise images, save them to src/public/src/images, and run:

    gulp images

Note that this command does not watch for image changes.

# Future plans...

- [View the MVP trello board](https://trello.com/b/nYrHaB9w/strava-challenges-mvp)
- What happens after MVP? [View the V1.1 trello board](https://trello.com/b/UGB13DOF/strava-challenges-1-1-upcoming)
