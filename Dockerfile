FROM golang:1.4.2

WORKDIR /go/src/github.com/ashsmith/strava-challenges
# Grab the source code and add it to the workspace.
# ADD . /go/src/github.com/ashsmith/strava-challenges

COPY bin/* /usr/local/bin/

VOLUME ["/go/src/github.com/ashsmith/strava-challenges"]

RUN ["/usr/local/bin/install-dependencies"]

CMD ["/usr/local/bin/start"]
